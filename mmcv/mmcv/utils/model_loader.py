import torch
import torch.nn as nn

def check_predictor(key):
    if "rpn_cls" or "rpn_reg" or "fc_cls" or "fc_reg" in key:
        return True
    else:
        return False

def load_incremental_model(model, saved_state_dict):
    model_dict = model.state_dict()
    for key, val in model_dict.items():
        if key in saved_state_dict:
            old_val = saved_state_dict[key]
            if len(old_val.size())>0:
                pred_channel = old_val.size(0)
                model_dict[key][:pred_channel] = old_val
            else:
                model_dict[key] = old_val
        else:
            print("{} not in state_dict".format(key))
    model.load_state_dict(model_dict)
    return None
