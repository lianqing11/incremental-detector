annotation_num = 3939


image_list = []
image_id_list = []

import random
import copy
import json
from tqdm import tqdm

ann_all = json.load(open("trainval.json"))


annotations = []
new_image_list=  []
image_list_all = ann_all['images']
image_num_dict = {image_id['id']:0 for image_id in image_list_all}
for i in ann_all['annotations']:
    image_num_dict[i['image_id']]+=1
current_ann_num = 0
while current_ann_num < annotation_num:
    img = image_list_all.pop()
    new_image_list.append(img)
    current_ann_num+=image_num_dict[img['id']]

new_ann = []
new_image_id = [i['id'] for i in new_image_list]
for i in tqdm(ann_all['annotations']):
    if i['image_id'] in new_image_id:
        new_ann.append(i)
ann_new_json = copy.deepcopy(ann_all)

ann_new_json['annotations'] = new_ann
ann_new_json['images'] = new_image_list
with open("trainval_ann_{}.json".format(annotation_num), 'w') as f:
    json_str = json.dumps(ann_new_json)
    f.write(json_str)

ann_new_json['images'] = image_list_all
ann_new_json['annotations'] = []
with open("trainval_ann_{}_unlabeled.json".format(annotation_num), 'w') as f:
    json_str = json.dumps(ann_new_json)
    f.write(json_str)



