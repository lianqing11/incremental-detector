import json
import copy
import numpy as np
from tqdm import tqdm
train_file = "instances_train2017"
ann = json.load(open('{}.json'.format(train_file)))
class_id = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
            27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39,
            40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62,
            63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77,
            78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]

class_id = np.asarray(class_id)

split_num = 4
class_num = 80
class_per_num = int(class_num / split_num)
image = ann['images']

def shuffle_and_group(array, n):
    groups = []
    shuf = array
    #ensuring no-empty list
    for i in range(n):
        groups.append([shuf.pop()])

    for num in shuf:
        groups[np.random.randint(n)].append(num)

    return groups
image_group = shuffle_and_group(image, split_num)

#build a huge annotation list
ann_all = []
img_all = []
ann_save = copy.deepcopy(ann)
for i in range(split_num):

    # for annotation
    #1. select the annotation with that image

    #2. filter the annotation outside the class space

    class_min = i*class_per_num+1
    class_max = (i+1)*class_per_num
    class_margin = class_id[class_min-1:class_max]
    ann_save['images'] = image_group[i]
    image_id = [k['id'] for k in ann_save['images']]
    image_list = ann_save['images']
    ann_per = ann['annotations']
    ann_per_new = []
    for k in tqdm(ann_per):
        if k['image_id'] in image_id:
            if k['category_id'] in class_margin:
                ann_per_new.append(k)


    for k, img in enumerate(image_list):
        img['category_domain'] = [class_min-1, class_max]
        image_list[k] = img
    img_all.extend(image_list)


    ann_all.extend(ann_per_new)
    ann_save['annotations'] = ann_per_new
    ann_save['images'] = image_list


    # save annotation
    with open("{}_split_{}_{}.json".format(train_file, split_num, i), 'w') as json_fp:
        json_str = json.dumps(ann_save)
        json_fp.write(json_str)

ann_save['images'] = img_all
ann_save['annotations'] = ann_all
print("original num / final num {}/{}".format(len(ann['annotations']), len(ann_all)))
with open("{}_split_{}.json".format(train_file, split_num), 'w') as json_fp:
    json_str = json.dumps(ann_save)
    json_fp.write(json_str)

