import json
import copy
import numpy as np
ann = json.load(open('trainval.json'))

split_num = 4
class_num = 20
class_per_num = int(class_num / split_num)
image = ann['images']

def shuffle_and_group(array, n):
    groups = []
    shuf = array
    #ensuring no-empty list
    for i in range(n):
        groups.append([shuf.pop()])

    for num in shuf:
        groups[np.random.randint(n)].append(num)

    return groups
image_group = shuffle_and_group(image, split_num)

#build a huge annotation list
ann_all = []
ann_save = copy.deepcopy(ann)
for i in range(split_num):

    # for annotation
    #1. select the annotation with that image

    #2. filter the annotation outside the class space

    class_min = i*class_per_num+1
    class_max = (i+1)*class_per_num
    ann_save['images'] = image_group[i]
    image_list = [k['id'] for k in ann_save['images']]
    ann_per = ann['annotations']
    ann_per_new = []
    for k in ann_per:
        if k['image_id'] in image_list:
            if class_min <= k['category_id'] <=class_max:
                ann_per_new.append(k)

    ann_all.extend(ann_per_new)
    ann_save['annotations'] = ann_per_new


    # save annotation
    with open("trainval_split_{}_{}.json".format(split_num, i), 'w') as json_fp:
        json_str = json.dumps(ann_save)
        json_fp.write(json_str)

ann_save['images'] = ann['images']
ann_save['annotations'] = ann_all
print("original num / final num {}/{}".format(len(ann['annotations']), len(ann_all)))
with open("trainval_split_{}.json".format(split_num), 'w') as json_fp:
    json_str = json.dumps(ann_save)
    json_fp.write(json_str)

