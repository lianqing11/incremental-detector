import json
import numpy as np

ann_all = json.load(open("instances_train2017.json"))
ann_all_split = ann_all.copy()
class_id = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
            27, 28, 31, 32, 33, 34, 35, 36, 37, 38, 39,
            40, 41, 42, 43, 44, 46, 47, 48, 49, 50, 51,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62,
            63, 64, 65, 67, 70, 72, 73, 74, 75, 76, 77,
            78, 79, 80, 81, 82, 84, 85, 86, 87, 88, 89, 90]

task_num = 4
class_num = len(class_id)/ 4
instance_list = []
for i in range(task_num):
    class_max = class_id[int((i+1)*class_num-1)]
    class_min = class_id[int(i*class_num)]
    instance_list = []
    print(class_min, class_max)
    for instance in ann_all['annotations']:
        if class_min <= instance['category_id'] <= class_max:
            instance_list.append(instance)
    ann_all_split['annotations'] = instance_list
    with open("instances_train_2017_{}.json".format(i+1), 'w') as f:

        dump_file = json.dumps(ann_all_split)
        f.write(dump_file)
