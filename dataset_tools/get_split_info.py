import json
split_num = 4
num_class = 20
num_class_per = int(num_class / split_num)
ann_all = json.load(open("trainval_split_{}.json".format(split_num)))

ann_split = []
img_split = []
img_key = []
for i in range(split_num):
    img_key_per = []
    ann_split.append(json.load(open("trainval_split_{}_{}.json".format(split_num, i))))
    for j in ann_split[i]['images']:
        j['category_domain'] = [i*num_class_per, (i+1)*num_class_per]
        img_key.append(j)
        img_key_per.append(j)
    ann_split[i]['images'] = img_key_per



ann_all['images'] = img_key

with open("trainval_split_{}_group.json".format(split_num), 'w') as fp:
    json_str = json.dumps(ann_all)
    fp.write(json_str)


for i in range(split_num):
    with open("trainval_split_{}_{}_group.json".format(split_num, i), 'w') as fp:
        json_str = json.dumps(ann_split[i])
        fp.write(json_str)
