#!/bin/bash
for teacher_alpha in {1..6}
do
    echo $teacher_alpha
    bash ./tools/dist_train.sh train_ssd_distill_weight.py incremental_cfgs/retinanet/distillation/distill_weight_1x.py 2 15323 --lambda_distill $teacher_alpha --work_dir tune_hyperparam/retinanet_distill_weight_lambda_distill-$teacher_alpha
done

