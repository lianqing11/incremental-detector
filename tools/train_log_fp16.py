from __future__ import division
import os.path as osp
import sys
import time
sys.path.insert(0, osp.join(osp.dirname(osp.abspath(__file__)), '../'))
sys.path.insert(0, osp.join(osp.dirname(osp.abspath(__file__)), '../mmcv'))
import re

import pdb

import argparse
from mmcv import Config

from mmdet import __version__
from mmdet.datasets import build_dataset
from mmdet.core import AverageMeter
from mmcv.runner import obj_from_dict
from collections import OrderedDict
import numpy as np
import os
import shutil
import mmcv
from mmcv.runner import init_dist
from mmdet.apis import (train_detector, get_root_logger,
                        set_random_seed)
from mmdet.models import build_detector
from mmcv.parallel import scatter, collate
from mmcv.runner import load_checkpoint
from mmcv.utils import mkdir_or_exist
import torch
from mmcv.parallel import MMDataParallel, MMDistributedDataParallel

from mmdet.core import (DistOptimizerHook, DistEvalmAPHook,
                        CocoDistEvalRecallHook, CocoDistEvalmAPHook)
from mmdet.core import LocalFp16OptimizerHook, LocalDistOptimizerHook
from mmdet.datasets import build_dataloader
from mmdet.models import RPN

from mmdet import datasets
from mmcv.runner.hooks import lr_updater
from mmdet.core import allreduce_grads
from torch.nn.utils import clip_grad
from mmdet.core.evaluation import evaluate
from mmdet.core import eval_map

import torch.distributed as dist
def parse_args():
    parser = argparse.ArgumentParser(description='Train a detector')
    parser.add_argument('config', help='train config file path')
    parser.add_argument('--work_dir', help='the dir to save logs and models')
    parser.add_argument(
        '--resume_from', help='the checkpoint file to resume from')
    parser.add_argument(
        '--validate',
        action='store_true',
        help='whether to evaluate the checkpoint during training')
    parser.add_argument(
        '--gpus',
        type=int,
        default=1,
        help='number of gpus to use '
        '(only applicable to non-distributed training)')
    parser.add_argument('--seed', type=int, default=None, help='random seed')
    parser.add_argument(
        '--launcher',
        choices=['none', 'pytorch', 'slurm', 'mpi'],
        default='none',
        help='job launcher')
    parser.add_argument('--local_rank', type=int, default=0)
    parser.add_argument(
        '--autoscale-lr',
        action='store_true',
        help='automatically scale lr with the number of gpus')
    args = parser.parse_args()
    if 'LOCAL_RANK' not in os.environ:
        os.environ['LOCAL_RANK'] = str(args.local_rank)


    return args
def clip_grads(params, grad_clip):
    clip_grad.clip_grad_norm_(
        filter(lambda p: p.requires_grad, params), **grad_clip)


def parse_losses(losses):
    log_vars = OrderedDict()
    for loss_name, loss_value in losses.items():
        if isinstance(loss_value, torch.Tensor):
            log_vars[loss_name] = loss_value.mean()
        elif isinstance(loss_value, list):
            log_vars[loss_name] = sum(_loss.mean() for _loss in loss_value)
        else:
            raise TypeError(
                '{} is not a tensor or list of tensors'.format(loss_name))

    loss = sum(_value for _key, _value in log_vars.items() if 'loss' in _key)

    log_vars['loss'] = loss
    for loss_name, loss_value in log_vars.items():
        # reduce loss when distributed training
        if dist.is_initialized():
            loss_value = loss_value.data.clone()
            dist.all_reduce(loss_value.div_(dist.get_world_size()))
        log_vars[loss_name] = loss_value.item()

    return loss, log_vars


def save_checkpoint(save_model, which_model, i_iter, cfg, is_best=True):
    i_iter = str(i_iter)
    suffix = '{}_i_iter'.format(which_model)
    dict_model = save_model.state_dict()
    print(cfg.work_dir + suffix)
    filename = osp.join(cfg.work_dir, suffix)
    torch.save(dict_model, filename + i_iter + '_checkpoint.pth.tar')
    if is_best:
        shutil.copyfile(filename + i_iter + '_checkpoint.pth.tar', osp.join(cfg.work_dir, 'best.pth'))
    shutil.copyfile(filename + i_iter + '_checkpoint.pth.tar', osp.join(cfg.work_dir, 'latest.pth'))

def get_dist_info():
    if torch.__version__ < '1.0':
        initialized = dist._initialized
    else:
        initialized = dist.is_initialized()
    if initialized:
        rank = dist.get_rank()
        world_size = dist.get_world_size()
    else:
        rank = 0
        world_size = 1
    return rank, world_size



def build_optimizer(model, optimizer_cfg):
    """Build optimizer from configs.

    Args:
        model (:obj:`nn.Module`): The model with parameters to be optimized.
        optimizer_cfg (dict): The config dict of the optimizer.
            Positional fields are:
                - type: class name of the optimizer.
                - lr: base learning rate.
            Optional fields are:
                - any arguments of the corresponding optimizer type, e.g.,
                  weight_decay, momentum, etc.
                - paramwise_options: a dict with 3 accepted fileds
                  (bias_lr_mult, bias_decay_mult, norm_decay_mult).
                  `bias_lr_mult` and `bias_decay_mult` will be multiplied to
                  the lr and weight decay respectively for all bias parameters
                  (except for the normalization layers), and
                  `norm_decay_mult` will be multiplied to the weight decay
                  for all weight and bias parameters of normalization layers.

    Returns:
        torch.optim.Optimizer: The initialized optimizer.
    """
    if hasattr(model, 'module'):
        model = model.module

    optimizer_cfg = optimizer_cfg.copy()
    paramwise_options = optimizer_cfg.pop('paramwise_options', None)
    # if no paramwise option is specified, just use the global setting
    if paramwise_options is None:
        return obj_from_dict(optimizer_cfg, torch.optim,
                             dict(params=model.parameters()))
    else:
        assert isinstance(paramwise_options, dict)
        # get base lr and weight decay
        base_lr = optimizer_cfg['lr']
        base_wd = optimizer_cfg.get('weight_decay', None)
        # weight_decay must be explicitly specified if mult is specified
        if ('bias_decay_mult' in paramwise_options
                or 'norm_decay_mult' in paramwise_options):
            assert base_wd is not None
        # get param-wise options
        bias_lr_mult = paramwise_options.get('bias_lr_mult', 1.)
        bias_decay_mult = paramwise_options.get('bias_decay_mult', 1.)
        norm_decay_mult = paramwise_options.get('norm_decay_mult', 1.)
        # set param-wise lr and weight decay
        params = []
        for name, param in model.named_parameters():
            param_group = {'params': [param]}
            if not param.requires_grad:
                # FP16 training needs to copy gradient/weight between master
                # weight copy and model weight, it is convenient to keep all
                # parameters here to align with model.parameters()
                params.append(param_group)
                continue

            # for norm layers, overwrite the weight decay of weight and bias
            # TODO: obtain the norm layer prefixes dynamically
            if re.search(r'(bn|gn)(\d+)?.(weight|bias)', name):
                if base_wd is not None:
                    param_group['weight_decay'] = base_wd * norm_decay_mult
            # for other layers, overwrite both lr and weight decay of bias
            elif name.endswith('.bias'):
                param_group['lr'] = base_lr * bias_lr_mult
                if base_wd is not None:
                    param_group['weight_decay'] = base_wd * bias_decay_mult
            # otherwise use the global settings

            params.append(param_group)

        optimizer_cls = getattr(torch.optim, optimizer_cfg.pop('type'))
        return optimizer_cls(params, **optimizer_cfg)



def main():
    args = parse_args()

    cfg = Config.fromfile(args.config)
    # set cudnn_benchmark
    if cfg.get('cudnn_benchmark', False):
        torch.backends.cudnn.benchmark = True
    # update configs according to CLI args
    if args.work_dir is not None:
        cfg.work_dir = args.work_dir
    if args.resume_from is not None:
        cfg.resume_from = args.resume_from
    cfg.gpus = args.gpus
    if cfg.checkpoint_config is not None:
        # save mmdet version in checkpoints as meta data
        cfg.checkpoint_config.meta = dict(
            mmdet_version=__version__, config=cfg.text)

    # init distributed env first, since logger depends on the dist info.
    if args.launcher == 'none':
        distributed = False
    else:
        distributed = True
        init_dist(args.launcher, **cfg.dist_params)

    # init logger before other steps
    mkdir_or_exist(cfg.work_dir)
    logger = get_root_logger(osp.join(cfg.work_dir, 'log.txt'), cfg.log_level)
    logger.info('Distributed training: {}'.format(distributed))
    for key, item in cfg.items():
        logger.info("{} : {}".format(key, item))
    print("args gpu {}".format(args.gpus))

    # set random seeds
    if args.seed is not None:
        logger.info('Set random seed to {}'.format(args.seed))
        set_random_seed(args.seed)

    model = build_detector(
        cfg.model, train_cfg=cfg.train_cfg, test_cfg=cfg.test_cfg)

    train_dataset = build_dataset(cfg.data.train)

    model = MMDistributedDataParallel(model.cuda())

    if cfg.resume_from:
        model.load_state_dict(torch.load(cfg.resume_from))
    model.train()


    #eval dataset
    eval_datasets = {}
    if 'name' not in cfg.data.val:
        cfg.data.val.name = 'val'
    eval_datasets[cfg.data.val.name]= obj_from_dict(cfg.data.val, datasets,
                                 {'test_mode': True})


    train_loader = build_dataloader(
            train_dataset,
            cfg.data.imgs_per_gpu,
            cfg.data.workers_per_gpu,
            dist=True)
    #build optimizer

    optimizer = build_optimizer(model, cfg.optimizer)
    fp16_cfg = cfg.get('fp16', None)
    if fp16_cfg is not None:
        optimizer_config = LocalFp16OptimizerHook(**cfg.optimizer_config, **fp16_cfg)
    else:
        optimizer_config = LocalDistOptimizerHook(**cfg.optimizer_config)

    rank, world_size = get_dist_info()

    current_epoch = 0
    full_batch_size=world_size * cfg.data.imgs_per_gpu
    if 'max_iters' not in cfg:
        iter_per_epoch = int(len(train_dataset)/full_batch_size)
        cfg.max_iters = iter_per_epoch * cfg.total_epochs
    if 'step' in cfg.lr_config and 'by_iter' not in cfg.lr_config:
        cfg.lr_config.step = [i*iter_per_epoch for i in cfg.lr_config.step]

    if 'eval_iter' not in cfg:
        cfg.eval_iter = int(len(train_dataset) / full_batch_size)
        print(cfg.eval_iter)
    # lr config
    if 'warmup' not in cfg.lr_config:
        cfg.lr_config.warmup = None
        cfg.lr_config.warmup_iters=1
        cfg.lr_config.warmup_ratio=0.1

    if cfg.lr_config.policy is 'cosine':
        lr_config = lr_updater.CosineLrUpdaterHook(warmup=cfg.lr_config.warmup,
                                                   warmup_iters=cfg.lr_config.warmup_iters,
                                                   warmup_ratio=cfg.lr_config.warmup_ratio)
    elif cfg.lr_config.policy is 'step':
        lr_config = lr_updater.StepLrUpdaterHook(step=cfg.lr_config.step,
                                                    warmup=cfg.lr_config.warmup,
                                                   warmup_iters=cfg.lr_config.warmup_iters,
                                                   warmup_ratio=cfg.lr_config.warmup_ratio)
    else:
        lr_config = lr_updater.FixedLrUpdaterHook(warmup=cfg.lr_config.warmup,
                                                   warmup_iters=cfg.lr_config.warmup_iters,
                                                   warmup_ratio=cfg.lr_config.warmup_ratio)

    lr_config.initialize(optimizer)
    if mmcv.is_str(cfg.work_dir):
        work_dir = osp.abspath(cfg.work_dir)
        mmcv.mkdir_or_exist(work_dir)
    elif cfg.work_dir is None:
        work_dir = None
    else:
        raise TypeError("work_dir not found")
    if hasattr(model, 'modules'):
        _model_name = model.module.__class__.__name__
    else:
        _model_name = model.__class__.__name__

    loss_value = AverageMeter(50)
    loss_cls_value = AverageMeter(50)
    batch_time = AverageMeter(50)
    data_time = AverageMeter(50)
    acc_value = AverageMeter(50)
    train_loader_iter = enumerate(train_loader)
    is_best = False
    best_mAP = -1
    if fp16_cfg is not None:
        optimizer_config.before_run(model, optimizer)

    lr = optimizer.param_groups[0]['lr']
    logger.info("lr {}".format(lr))

    for iter_index in range(cfg.max_iters):
        end = time.time()
        try:
            _, data_batch = train_loader_iter.__next__()
        except StopIteration:
            current_epoch+=1
            train_loader.sampler.set_epoch(current_epoch)
            train_loader_iter = enumerate(train_loader)
            _, data_batch = train_loader_iter.__next__()

        data_time.update(time.time() - end)
        lr_config.update(optimizer, iter_index, cfg.max_iters)

        #begin inference
        losses = model(**data_batch)
        loss, log_vars = parse_losses(losses)
        optimizer_config.after_train_iter(model, optimizer, loss)

        loss_value.update(log_vars['loss'])
        loss_cls_value.update(log_vars['loss_cls'])
        batch_time.update(time.time() - end)
        remain_iter = cfg.max_iters - iter_index
        remain_time = remain_iter * batch_time.avg
        t_m, t_s = divmod(remain_time, 60)
        t_h, t_m = divmod(t_m, 60)
        remain_time = '{:02d}:{:02d}:{:02d}'.format(int(t_h), int(t_m), int(t_s))
        # update learning rate


        if iter_index % 50 == 0:
            loss_info = ''
            for loss_key, loss_val in log_vars.items():
                loss_info += "{} = {:.3f}\t".format(loss_key, loss_val)
            lr = optimizer.param_groups[0]['lr']
            logger.info("lr {}".format(lr))
            logger.info("Iter = {iter} | {max_iter}\t"
                        "Epch = {current_epoch} | {total_epoch}\t"
                        "Time = {batch_time.avg:.4f}\t"
                        "remain_time = {remain_time}\t"
                        "data_time = {data_time.avg:.4f}\t"
                        "loss = {loss.avg:.4f}\t"
                        "loss_cls = {loss_cls.avg:.4f}\t"
                        "{loss_info}".format(
                            iter = iter_index,
                            current_epoch=current_epoch,
                            total_epoch=cfg.total_epochs,
                            loss = loss_value,
                            loss_cls = loss_cls_value,
                            max_iter = cfg.max_iters,
                            batch_time=batch_time,
                            data_time=data_time,
                            remain_time=remain_time,
                            loss_info=loss_info))
        if (iter_index+1) % cfg.eval_iter == 0:
            # save checkpoint
            if cfg.dataset_type == 'VOCDataset':
                for name, eval_dataset in eval_datasets.items():
                    det_results = [None for _ in range(len(eval_dataset))]
                    if rank == 0:
                        prog_bar = mmcv.ProgressBar(len(eval_dataset))
                    if rank == 0:
                        model.eval()
                        for idx in range(rank, len(eval_dataset)):
                            data = eval_dataset[idx]
                            data_gpu = scatter(
                                collate([data], samples_per_gpu=1),
                                [torch.cuda.current_device()])[0]
                            with torch.no_grad():
                                result = model(return_loss=False, rescale=True, **data_gpu)
                            det_results[idx] = result
                            for _ in range(1):
                                prog_bar.update()
                        print("\n")
                        #eval metrics for voc
                        gt_bboxes = []
                        gt_labels = []
                        gt_ignore = []
                        for i in range(len(eval_dataset)):
                            ann = eval_dataset.get_ann_info(i)
                            bboxes = ann['bboxes']
                            labels = ann['labels']
                            if 'bboxes_ignore' in ann:
                                ignore = np.concatenate([
                                    np.zeros(bboxes.shape[0], dtype=np.bool),
                                    np.ones(ann['bboxes_ignore'].shape[0], dtype=np.bool)
                                ])
                                gt_ignore.append(ignore)
                                bboxes = np.vstack([bboxes, ann['bboxes_ignore']])
                                labels = np.concatenate([labels, ann['labels_ignore']])
                            gt_bboxes.append(bboxes)
                            gt_labels.append(labels)

                        if not gt_ignore:
                            gt_ignore = gt_ignore
                        if hasattr(eval_dataset, 'year') and eval_dataset.year == 2007:
                            dataset_name = 'voc07'
                        else:
                            dataset_name = eval_dataset.CLASSES
                        mean_ap, eval_results = eval_map(
                                            det_results,
                                            gt_bboxes,
                                            gt_labels,
                                            gt_ignore=gt_ignore,
                                            scale_ranges = None,
                                            iou_thr=0.5,
                                            dataset = dataset_name,
                                            print_summary=True)
                        logger.info("index : {} mean ap: {}".format(iter_index, mean_ap))
                        if mean_ap > best_mAP:
                            is_best = True
                            best_mAP = mean_ap
                        logger.info("index : {} best mean ap: {}".format(iter_index, best_mAP))
                        mean_ap_0_10 = 0
                        mean_ap_10_20 = 0
                        for i in range(10):
                            mean_ap_0_10+=eval_results[i]['ap']
                            mean_ap_10_20+=eval_results[i+10]['ap']
                        mean_ap_0_5 = 0
                        mean_ap_5_10 = 0
                        mean_ap_10_15 = 0
                        mean_ap_15_20 = 0
                        for i in range(5):
                            mean_ap_0_5+= eval_results[i]['ap']/5.
                            mean_ap_5_10+= eval_results[i+5]['ap']/5.
                            mean_ap_10_15+= eval_results[i+10]['ap']/5.
                            mean_ap_15_20+= eval_results[i+15]['ap']/5.

                        logger.info("mean ap 0-10 {}".format(mean_ap_0_10))
                        logger.info("mean ap 10-20 {}".format(mean_ap_10_20))
                        logger.info("mean ap 0-5 {}".format(mean_ap_0_5))
                        logger.info("mean ap 5-10 {}".format(mean_ap_5_10))
                        logger.info("mean ap 10-15 {}".format(mean_ap_10_15))
                        logger.info("mean ap 15-20 {}".format(mean_ap_15_20))

                        for i in range(20):
                            logger.info("index {}: ap: {}".format(i+1, eval_results[i]['ap']))
                        model.train()
            else:
                for name, eval_dataset in eval_datasets.items():
                    det_results = [None for _ in range(len(eval_dataset))]
                    if rank == 0:
                        prog_bar = mmcv.ProgressBar(len(eval_dataset))
                    if rank == 0:
                        model.eval()
                        for idx in range(rank, len(eval_dataset)):
                            data = eval_dataset[idx]
                            data_gpu = scatter(
                                collate([data], samples_per_gpu=1),
                                [torch.cuda.current_device()])[0]
                            with torch.no_grad():
                                result = model(return_loss=False, rescale=True,**data_gpu)
                            det_results[idx] = result
                            for _ in range(1):
                                prog_bar.update()
                        print("finish \n")
                        eval_metrics = evaluate(cfg.work_dir, eval_dataset, det_results)
                        logger.info("index: {}".format(iter_index))
                        for key, item in eval_metrics.items():
                            logger.info("{} : {}".format(key, item))
                        current_mAP = eval_metrics['bbox_mAP']
                        model.train()
                        if current_mAP > best_mAP:
                            is_best = True
                            best_mAP = current_mAP

            dist.barrier()
            if rank == 0:
                save_checkpoint(model, 'model', iter_index, cfg, is_best)
                is_best = False
            model.train()







if __name__ == '__main__':
    main()
