#!/usr/bin/env bash

PYTHON=${PYTHON:-"python"}

CONFIG=$2
GPUS=$3
for i in {2..4}
do
    echo $i
    $PYTHON -m torch.distributed.launch --nproc_per_node=$GPUS  --master_port $4\
        $(dirname "$0")/$1 $CONFIG --task_idx $i --launcher pytorch ${@:6}
done
