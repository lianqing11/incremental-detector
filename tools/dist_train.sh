#!/usr/bin/env bash

PYTHON=${PYTHON:-"python"}

CONFIG=$2
GPUS=$3

$PYTHON -m torch.distributed.launch --nproc_per_node=$GPUS --master_port $4\
    $(dirname "$0")/$1 $CONFIG --launcher pytorch ${@:5}
