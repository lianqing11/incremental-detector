from argparse import ArgumentParser
import os.path as osp
import sys
import time
sys.path.insert(0, osp.join(osp.dirname(osp.abspath(__file__)), '../'))
sys.path.insert(0, osp.join(osp.dirname(osp.abspath(__file__)), '../mmcv'))
from mmdet.core import coco_eval
from mmdet.apis import get_root_logger


def main():
    parser = ArgumentParser(description='COCO Evaluation')
    parser.add_argument('result', help='result file path')
    parser.add_argument('--ann', help='annotation file path')
    parser.add_argument(
        '--types',
        type=str,
        nargs='+',
        choices=['proposal_fast', 'proposal', 'bbox', 'segm', 'keypoint'],
        default=['bbox'],
        help='result types')
    parser.add_argument(
        '--max-dets',
        type=int,
        nargs='+',
        default=[100, 300, 1000],
        help='proposal numbers, only used for recall evaluation')
    args = parser.parse_args()
    logger = get_root_logger('./work_dirs/eval_coco.txt')
    a = coco_eval(args.result, args.types, args.ann, args.max_dets)
    import pdb
    pdb.set_trace()


if __name__ == '__main__':
    main()
