from __future__ import division

import numpy as np
import torch
import torch.nn as nn
from mmcv.cnn import normal_init

from mmdet.core import (AnchorGenerator, anchor_target, delta2bbox, force_fp32,
                        multi_apply, multiclass_nms, anchor_target_ignore, anchor_target_ignore_v1)
from ..builder import build_loss
from ..registry import HEADS


@HEADS.register_module
class AnchorHead(nn.Module):
    """Anchor-based head (RPN, RetinaNet, SSD, etc.).

    Args:
        num_classes (int): Number of categories including the background
            category.
        in_channels (int): Number of channels in the input feature map.
        feat_channels (int): Number of hidden channels. Used in child classes.
        anchor_scales (Iterable): Anchor scales.
        anchor_ratios (Iterable): Anchor aspect ratios.
        anchor_strides (Iterable): Anchor strides.
        anchor_base_sizes (Iterable): Anchor base sizes.
        target_means (Iterable): Mean values of regression targets.
        target_stds (Iterable): Std values of regression targets.
        loss_cls (dict): Config of classification loss.
        loss_bbox (dict): Config of localization loss.
    """  # noqa: W605

    def __init__(self,
                 num_classes,
                 in_channels,
                 feat_channels=256,
                 anchor_scales=[8, 16, 32],
                 anchor_ratios=[0.5, 1.0, 2.0],
                 anchor_strides=[4, 8, 16, 32, 64],
                 anchor_base_sizes=None,
                 target_means=(.0, .0, .0, .0),
                 target_stds=(1.0, 1.0, 1.0, 1.0),
                 loss_cls=dict(
                     type='CrossEntropyLoss',
                     use_sigmoid=True,
                     loss_weight=1.0),
                 loss_bbox=dict(
                     type='SmoothL1Loss', beta=1.0 / 9.0, loss_weight=1.0,
                    ), **kwargs):
        super(AnchorHead, self).__init__()
        self.in_channels = in_channels
        self.num_classes = num_classes
        self.feat_channels = feat_channels
        self.anchor_scales = anchor_scales
        self.anchor_ratios = anchor_ratios
        self.anchor_strides = anchor_strides
        self.anchor_base_sizes = list(
            anchor_strides) if anchor_base_sizes is None else anchor_base_sizes
        self.target_means = target_means
        self.target_stds = target_stds

        self.use_sigmoid_cls = loss_cls.get('use_sigmoid', False)
        self.sampling = loss_cls['type'] not in ['FocalLoss', 'GHMC']
        if self.use_sigmoid_cls:
            self.cls_out_channels = num_classes - 1
        else:
            self.cls_out_channels = num_classes

        if self.cls_out_channels <= 0:
            raise ValueError('num_classes={} is too small'.format(num_classes))

        self.loss_cls = build_loss(loss_cls)
        self.loss_bbox = build_loss(loss_bbox)
        self.fp16_enabled = False

        self.anchor_generators = []
        for anchor_base in self.anchor_base_sizes:
            self.anchor_generators.append(
                AnchorGenerator(anchor_base, anchor_scales, anchor_ratios))

        self.num_anchors = len(self.anchor_ratios) * len(self.anchor_scales)
        self._init_layers()

    def _init_layers(self):
        self.conv_cls = nn.Conv2d(self.in_channels,
                                  self.num_anchors * self.cls_out_channels, 1)
        self.conv_reg = nn.Conv2d(self.in_channels, self.num_anchors * 4, 1)

    def init_weights(self):
        normal_init(self.conv_cls, std=0.01)
        normal_init(self.conv_reg, std=0.01)

    def forward_single(self, x):
        cls_score = self.conv_cls(x)
        bbox_pred = self.conv_reg(x)
        return cls_score, bbox_pred

    def forward(self, feats):
        return multi_apply(self.forward_single, feats)

    def get_anchors(self, featmap_sizes, img_metas, device='cuda'):
        """Get anchors according to feature map sizes.

        Args:
            featmap_sizes (list[tuple]): Multi-level feature map sizes.
            img_metas (list[dict]): Image meta info.
            device (torch.device | str): device for returned tensors

        Returns:
            tuple: anchors of each image, valid flags of each image
        """
        num_imgs = len(img_metas)
        num_levels = len(featmap_sizes)

        # since feature map sizes of all images are the same, we only compute
        # anchors for one time
        multi_level_anchors = []
        for i in range(num_levels):
            anchors = self.anchor_generators[i].grid_anchors(
                featmap_sizes[i], self.anchor_strides[i], device=device)
            multi_level_anchors.append(anchors)
        anchor_list = [multi_level_anchors for _ in range(num_imgs)]

        # for each image, we compute valid flags of multi level anchors
        valid_flag_list = []
        for img_id, img_meta in enumerate(img_metas):
            multi_level_flags = []
            for i in range(num_levels):
                anchor_stride = self.anchor_strides[i]
                feat_h, feat_w = featmap_sizes[i]
                h, w, _ = img_meta['pad_shape']
                valid_feat_h = min(int(np.ceil(h / anchor_stride)), feat_h)
                valid_feat_w = min(int(np.ceil(w / anchor_stride)), feat_w)
                flags = self.anchor_generators[i].valid_flags(
                    (feat_h, feat_w), (valid_feat_h, valid_feat_w),
                    device=device)
                multi_level_flags.append(flags)
            valid_flag_list.append(multi_level_flags)

        return anchor_list, valid_flag_list

    def loss_single_filter(self, cls_score, bbox_pred, labels, label_weights,
                    bbox_targets, bbox_weights,
                    labels_filter, label_weights_filter, bbox_targets_filter, bbox_weights_filter,
                    num_total_samples, cfg,
                    img_metas=None):
        # classification loss
        labels_shape = labels.shape
        labels = labels.reshape(-1)
        label_weights = label_weights.reshape(-1)
        cls_score = cls_score.permute(0, 2, 3,
                                      1).reshape(-1, self.cls_out_channels)
        loss_cls = self.loss_cls(
            cls_score, labels, label_weights, avg_factor=num_total_samples)
        if self.loss_cls.reduction == "none":
            if 'clean_foreground' in cfg:
                labels_filter = labels_filter.reshape(-1)
                label_weights_filter = label_weights_filter.reshape(-1)
                loss_cls_filter = self.loss_cls(
                    cls_score, labels_filter, label_weights_filter, avg_factor=num_total_samples)
                loss_cls_1 = loss_cls_filter[labels_filter!=0][:].sum()
                loss_cls_0_in_domain = 0
                loss_cls_0_out_domain = 0
                labels = labels.reshape(labels_shape)
                loss_cls_filter = loss_cls_filter.reshape([labels_shape[0], labels_shape[1], loss_cls_filter.shape[-1]])
                loss_cls = loss_cls.reshape([labels_shape[0], labels_shape[1], loss_cls.shape[-1]])
                labels_filter = labels_filter.reshape(labels_shape)
                for bth in range(labels_shape[0]):
                    category_domain = img_metas[bth]['category_domain']
                    loss_cls_0_in_domain += loss_cls_filter[bth][labels_filter[bth]==0][:,category_domain[0]:category_domain[1]].sum()
                    loss_cls_0_out_domain += loss_cls[bth][labels[bth]==0][:,:category_domain[0]].sum()
                    loss_cls_0_out_domain += loss_cls[bth][labels[bth]==0][:,category_domain[1]:].sum()
                loss_cls_0 = loss_cls_0_in_domain + loss_cls_0_out_domain
                if 'norm_negative_loss' in cfg:
                    loss_cls_0 *= (loss_cls_0_in_domain.item() / (loss_cls_0.item() + 1e-7))
                elif 'norm_negative_loss1' in cfg:
                    loss_cls_0 *= (loss_cls[labels_filter==0].sum().item() / (loss_cls_0.item() + 1e-7))
            else:
                loss_cls_1 = loss_cls[labels!=0][:].sum()
                if 'new_class' in cfg:
                    loss_cls_0 = loss_cls[labels==0][:, cfg.new_class:].sum()
                else:
                    loss_cls_0 = 0
                    # calculate the nagative loss
                    loss_cls = loss_cls.reshape([labels_shape[0], labels_shape[1], loss_cls.shape[-1]])
                    labels = labels.reshape(labels_shape)
                    for bth in range(labels_shape[0]):
                        category_domain = img_metas[bth]['category_domain']
                        if len(category_domain) == 2:
                            loss_cls_0 += loss_cls[bth][labels[bth] == 0][:, category_domain[0]:category_domain[1]].sum()
                        else:
                            loss_cls_0 += loss_cls[bth][labels[bth] == 0][:, category_domain].sum()
            loss_cls = (loss_cls_1 + loss_cls_0)/num_total_samples
        # regression loss
        if 'clean_foreground' not in cfg:
            bbox_targets = bbox_targets.reshape(-1, 4)
            bbox_weights = bbox_weights.reshape(-1, 4)
            bbox_pred = bbox_pred.permute(0, 2, 3, 1).reshape(-1, 4)
            loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)
        else:
            bbox_targets_filter = bbox_targets_filter.reshape(-1, 4)
            bbox_weights_filter = bbox_weights_filter.reshape(-1, 4)
            bbox_pred = bbox_pred.permute(0, 2, 3, 1).reshape(-1, 4)
            loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets_filter,
                bbox_weights_filter,
                avg_factor=num_total_samples)

        return loss_cls, loss_bbox

    def distill_loss(self, output, old_output, threshold=None):
        curr = (output - old_output) ** 2
        if threshold is not None:
            k = torch.sigmoid(old_output)
            mask = (k<=threshold[0]) & (k >=threshold[1])
            curr = curr * mask.detach()
        return curr.sum()

    def loss_single_distill(self, cls_score, bbox_pred, labels, label_weights,
                    bbox_targets, bbox_weights, old_output_cls, old_output_reg,
                    num_total_samples,cfg,
                    labels_filter=None,
                    label_weights_filter=None,
                    bbox_targets_filter=None,
                    bbox_weights_filter=None,
                    img_metas=None):
        # classification loss
        labels_shape = labels.shape
        labels = labels.reshape(-1)
        label_weights = label_weights.reshape(-1)
        cls_score = cls_score.permute(0, 2, 3,
                                      1).reshape(-1, self.cls_out_channels)
        loss_cls = self.loss_cls(
            cls_score, labels, label_weights, avg_factor=num_total_samples)
        if self.loss_cls.reduction == "none":
            if not 'split_cls_only' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()
            else:
                loss_cls = loss_cls.reshape([labels_shape[0], labels_shape[1], loss_cls.shape[-1]])
                labels = labels.reshape(labels_shape)
                labels_mask = labels.clone()
                for bth in range(labels.shape[0]):
                    category_domain = img_metas[bth]['category_domain']
                    if len(category_domain) == 2:
                        labels_mask[bth][(labels_mask[bth]<=category_domain[0])|(labels_mask[bth]>category_domain[1])] = 0
                loss_cls_1 = loss_cls[labels_mask!=0].sum()
                loss_cls_1_scale = loss_cls[labels!=0].sum()
                loss_cls_1 = loss_cls_1 * (loss_cls_1_scale.item() / (loss_cls_1.item() + 1e-7))
            loss_cls_0 = 0
            # calculate the nagative loss
            loss_cls = loss_cls.reshape([labels_shape[0], labels_shape[1], loss_cls.shape[-1]])

            old_output_cls = old_output_cls.permute(0, 2, 3, 1).reshape(labels_shape[0], -1, self.cls_out_channels)
            cls_score = cls_score.reshape(labels_shape[0], -1, self.cls_out_channels)
            labels = labels.reshape(labels_shape)
            curr_loss = 0
            for bth in range(labels_shape[0]):
                category_domain = img_metas[bth]['category_domain']
                if len(category_domain) == 2:
                    loss_cls_0 += loss_cls[bth][labels[bth] == 0][:, category_domain[0]:category_domain[1]].sum()
                else:
                    loss_cls_0 += loss_cls[bth][labels[bth] == 0][:, category_domain].sum()
                # calculate the distillation loss
                if old_output_cls is not None:
                    if len(category_domain) == 2:
                        if 'distill_threshold' in cfg:
                            threshold = cfg.distill_threshold
                        else:
                            threshold = None
                        if 'distill_all' in cfg:
                            curr_loss += self.distill_loss(cls_score[bth], old_output_cls[bth], threshold=threshold)
                        else:
                            mask = labels[bth] == 0
                            curr_loss += self.distill_loss(cls_score[bth][mask][:, :category_domain[0]], old_output_cls[bth][mask][:, :category_domain[0]], threshold=threshold)
                            curr_loss += self.distill_loss(cls_score[bth][mask][:, category_domain[1]:], old_output_cls[bth][mask][:, category_domain[1]:], threshold=threshold)

            if 'norm_negative_loss_coco' in cfg:
                loss_cls_0 *= (loss_cls[bth][labels[bth] == 0].sum().item() / (loss_cls_0.item() + 1e-7))
            loss_cls = (loss_cls_1 + loss_cls_0)/num_total_samples
            curr_loss /= (labels_shape[0]*labels_shape[1] * cls_score.size(-1))
        # regression loss
        if 'split_reg_only' in cfg:
            bbox_weights_mask = bbox_weights.clone()
            labels = labels.reshape(labels_shape)
            for bth in range(labels_shape[0]):
                category_domain = img_metas[bth]['category_domain']
                if len(category_domain) == 2:
                    mask = (labels[bth]<=category_domain[0]) | (labels[bth]>category_domain[1])
                    bbox_weights_mask[bth][mask] = 0
            bbox_weights_mask = bbox_weights_mask.reshape(-1, 4)
        bbox_targets = bbox_targets.reshape(-1, 4)
        bbox_weights = bbox_weights.reshape(-1, 4)
        bbox_pred = bbox_pred.permute(0, 2, 3, 1).reshape(-1, 4)
        if 'split_reg_only' in cfg:
            loss_bbox_scale = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)
            loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)
            loss_bbox = loss_bbox * (loss_bbox_scale.item() /(loss_bbox.item() + 1e-7))
        else:
            loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)

        return loss_cls, loss_bbox, curr_loss

    def loss_single(self, cls_score, bbox_pred, labels, label_weights,
                    bbox_targets, bbox_weights, num_total_samples,cfg,
                    labels_filter=None,
                    label_weights_filter=None,
                    bbox_targets_filter=None,
                    bbox_weights_filter=None,
                    img_metas=None):
        # classification loss
        labels_shape = labels.shape
        labels = labels.reshape(-1)
        label_weights = label_weights.reshape(-1)
        cls_score = cls_score.permute(0, 2, 3,
                                      1).reshape(-1, self.cls_out_channels)
        loss_cls = self.loss_cls(
            cls_score, labels, label_weights, avg_factor=num_total_samples)
        if self.loss_cls.reduction == "none":
            if not 'split_cls_only' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()
            else:
                loss_cls = loss_cls.reshape([labels_shape[0], labels_shape[1], loss_cls.shape[-1]])
                labels = labels.reshape(labels_shape)
                labels_mask = labels.clone()
                for bth in range(labels.shape[0]):
                    category_domain = img_metas[bth]['category_domain']
                    if len(category_domain) == 2:
                        labels_mask[bth][(labels_mask[bth]<=category_domain[0])|(labels_mask[bth]>category_domain[1])] = 0
                loss_cls_1 = loss_cls[labels_mask!=0].sum()
                loss_cls_1_scale = loss_cls[labels!=0].sum()
                #loss_cls_1 = loss_cls_1 * (loss_cls_1_scale.item() / (loss_cls_1.item() + 1e-7))
            loss_cls_0 = 0
            # calculate the nagative loss
            loss_cls = loss_cls.reshape([labels_shape[0], labels_shape[1], loss_cls.shape[-1]])
            labels = labels.reshape(labels_shape)
            curr_loss = 0
            for bth in range(labels_shape[0]):
                category_domain = img_metas[bth]['category_domain']
                if len(category_domain) == 2:
                    loss_cls_0 += loss_cls[bth][labels[bth] == 0][:, category_domain[0]:category_domain[1]].sum()
                else:
                    loss_cls_0 += loss_cls[bth][labels[bth] == 0][:, category_domain].sum()


                # calculate the distillation loss

            if 'norm_negative_loss_coco' in cfg:
                loss_cls_0 *= (loss_cls[bth][labels[bth] == 0].sum().item() / (loss_cls_0.item() + 1e-7))
            loss_cls = (loss_cls_1 + loss_cls_0)/num_total_samples
        # regression loss
        if 'split_reg_only' in cfg:
            bbox_weights_mask = bbox_weights.clone()
            labels = labels.reshape(labels_shape)
            for bth in range(labels_shape[0]):
                category_domain = img_metas[bth]['category_domain']
                if len(category_domain) == 2:
                    mask = (labels[bth]<=category_domain[0]) | (labels[bth]>category_domain[1])
                    bbox_weights_mask[bth][mask] = 0
            bbox_weights_mask = bbox_weights_mask.reshape(-1, 4)
        bbox_targets = bbox_targets.reshape(-1, 4)
        bbox_weights = bbox_weights.reshape(-1, 4)
        bbox_pred = bbox_pred.permute(0, 2, 3, 1).reshape(-1, 4)
        if 'split_reg_only' in cfg:
            loss_bbox_scale = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights_mask,
                avg_factor=num_total_samples)
            loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)
            loss_bbox = loss_bbox * (loss_bbox_scale.item() /(loss_bbox.item() + 1e-7))

        else:
            loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)

        return loss_cls, loss_bbox

    def loss_single_ignore_v1(self, cls_score, bbox_pred, labels, label_weights,
                    bbox_targets, bbox_weights,
                    labels_hard,
                    labels_invalid, label_weights_invalid,
                    bbox_targets_invalid, bbox_weights_invalid,
                    num_total_samples,cfg,
                    labels_filter=None,
                    label_weights_filter=None,
                    bbox_targets_filter=None,
                    bbox_weights_filter=None,
                    img_metas=None):
        # classification loss
        labels_shape = labels.shape
        labels = labels.reshape(-1)
        labels_invalid = labels_invalid.reshape(-1)
        label_weights = label_weights.reshape(-1)

        labels_hard = labels_hard.reshape(-1)
        cls_score = cls_score.permute(0, 2, 3,
                                      1).reshape(-1, self.cls_out_channels)
        loss_cls = self.loss_cls(
            cls_score, labels, label_weights, avg_factor=num_total_samples)
        num_imgs = labels_shape[0]
        loss_cls_invalid = self.loss_cls(
            cls_score, labels_invalid*-1, label_weights, avg_factor=num_total_samples)
        if self.loss_cls.reduction == "none":
            if 'oracle_ignore' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()

                # for loss_cls_0
                ignore_mask = (labels==0) & (labels==labels_invalid)
                ignore_mask[(labels_hard!=0) & (labels == 0)] = 1
                loss_cls_0 = loss_cls[ignore_mask].sum()
            elif 'oracle_negative' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()
                mask = (labels==0) & (labels!=labels_invalid)
                loss_cls_0 = loss_cls[labels==0].sum()
                invalid_idx = labels_invalid[mask]
                invalid_idx = [i*self.cls_out_channels + invalid_idx[i].item()*(-1) - 1 for i in range(len(invalid_idx))]
                loss_cls_0 -= loss_cls[mask].view(-1)[invalid_idx].sum()
            elif 'oracle_positive' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()
                mask = (labels==0) & (labels==labels_invalid)
                loss_cls_0 = loss_cls[mask].sum()
                conflict_mask = (labels==0) & (labels!=labels_invalid)
                loss_cls_1 += loss_cls_invalid[conflict_mask].sum()
            elif 'oracle_ignore_negative' in cfg:
                loss_cls = loss_cls.sum()
                labels_invalid = labels_invalid.reshape(labels_shape)

                for i in range(num_imgs):
                    ignore_example = torch.unique(labels_invalid[i])
                    ignore_example-=1
                    loss_cls -= loss_cls[i][:,ignore_example]
            else:
                loss_cls_1 = loss_cls[labels!=0].sum()
                loss_cls_0 = loss_cls[labels==0].sum()

            loss_cls = (loss_cls_1 + loss_cls_0)/num_total_samples
        # regression loss
        bbox_targets = bbox_targets.reshape(-1, 4)
        bbox_weights = bbox_weights.reshape(-1, 4)
        bbox_pred = bbox_pred.permute(0, 2, 3, 1).reshape(-1, 4)
        loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)


        if 'oracle_positive_reg' in cfg:
            bbox_targets_invalid = bbox_targets_invalid.reshape(-1,4)
            bbox_weights_invalid = bbox_weights_invalid.reshape(-1,4)
            loss_bbox += self.loss_bbox(
                bbox_pred,
                bbox_targets_invalid,
                bbox_weights_invalid,
                avg_factor=num_total_samples)
        return loss_cls, loss_bbox


    def loss_single_ignore(self, cls_score, bbox_pred, labels, label_weights,
                    bbox_targets, bbox_weights,
                    labels_invalid, label_weights_invalid,
                    bbox_targets_invalid, bbox_weights_invalid,
                    num_total_samples,cfg,
                    labels_filter=None,
                    label_weights_filter=None,
                    bbox_targets_filter=None,
                    bbox_weights_filter=None,
                    img_metas=None):
        # classification loss
        labels_shape = labels.shape
        labels = labels.reshape(-1)
        labels_invalid = labels_invalid.reshape(-1)
        label_weights = label_weights.reshape(-1)
        cls_score = cls_score.permute(0, 2, 3,
                                      1).reshape(-1, self.cls_out_channels)
        loss_cls = self.loss_cls(
            cls_score, labels, label_weights, avg_factor=num_total_samples)
        num_imgs = labels_shape[0]
        loss_cls_invalid = self.loss_cls(
            cls_score, labels_invalid*-1, label_weights, avg_factor=num_total_samples)
        if self.loss_cls.reduction == "none":
            if 'oracle_ignore' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()

                # for loss_cls_0
                loss_cls_0 = loss_cls[(labels==0) & (labels==labels_invalid)].sum()
            elif 'oracle_negative' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()
                mask = (labels==0) & (labels!=labels_invalid)
                loss_cls_0 = loss_cls[labels==0].sum()
                invalid_idx = labels_invalid[mask]
                invalid_idx = [i*self.cls_out_channels + invalid_idx[i].item()*(-1) - 1 for i in range(len(invalid_idx))]
                loss_cls_0 -= loss_cls[mask].view(-1)[invalid_idx].sum()
            elif 'oracle_positive' in cfg:
                loss_cls_1 = loss_cls[labels!=0].sum()
                mask = (labels==0) & (labels==labels_invalid)
                loss_cls_0 = loss_cls[mask].sum()
                conflict_mask = (labels==0) & (labels!=labels_invalid)
                loss_cls_1 += loss_cls_invalid[conflict_mask].sum()
            elif 'oracle_ignore_negative' in cfg:
                loss_cls = loss_cls.sum()
                labels_invalid = labels_invalid.reshape(labels_shape)

                for i in range(num_imgs):
                    ignore_example = torch.unique(labels_invalid[i])
                    ignore_example-=1
                    loss_cls -= loss_cls[i][:,ignore_example]
            else:
                loss_cls_1 = loss_cls[labels!=0].sum()
                loss_cls_0 = loss_cls[labels==0].sum()

            loss_cls = (loss_cls_1 + loss_cls_0)/num_total_samples
        # regression loss
        bbox_targets = bbox_targets.reshape(-1, 4)
        bbox_weights = bbox_weights.reshape(-1, 4)
        bbox_pred = bbox_pred.permute(0, 2, 3, 1).reshape(-1, 4)
        loss_bbox = self.loss_bbox(
                bbox_pred,
                bbox_targets,
                bbox_weights,
                avg_factor=num_total_samples)


        if 'oracle_positive_reg' in cfg:
            bbox_targets_invalid = bbox_targets_invalid.reshape(-1,4)
            bbox_weights_invalid = bbox_weights_invalid.reshape(-1,4)
            loss_bbox += self.loss_bbox(
                bbox_pred,
                bbox_targets_invalid,
                bbox_weights_invalid,
                avg_factor=num_total_samples)
        return loss_cls, loss_bbox


    @force_fp32(apply_to=('cls_scores', 'bbox_preds'))
    def loss_ignore(self,
             cls_scores,
             bbox_preds,
             gt_bboxes,
             gt_labels,
             img_metas,
             cfg,
             gt_bboxes_ignore=None,
             old_output=None):
        featmap_sizes = [featmap.size()[-2:] for featmap in cls_scores]
        assert len(featmap_sizes) == len(self.anchor_generators)

        device = cls_scores[0].device

        anchor_list, valid_flag_list = self.get_anchors(
            featmap_sizes, img_metas, device=device)
        label_channels = self.cls_out_channels if self.use_sigmoid_cls else 1
        cls_reg_targets, cls_reg_targets_ignore = anchor_target_ignore(
            anchor_list,
            valid_flag_list,
            gt_bboxes,
            img_metas,
            self.target_means,
            self.target_stds,
            cfg,
            gt_bboxes_ignore_list=gt_bboxes_ignore,
            gt_labels_list=gt_labels,
            label_channels=label_channels,
            sampling=self.sampling)
        if cls_reg_targets is None:
            return None
        (labels_list, label_weights_list, bbox_targets_list, bbox_weights_list,
        num_total_pos, num_total_neg) = cls_reg_targets
        (labels_list_invalid, label_weights_list_invalid,
         bbox_targets_list_invalid, bbox_weights_list_invalid) = cls_reg_targets_ignore
        num_total_samples = (
            num_total_pos + num_total_neg if self.sampling else num_total_pos)
        losses_cls, losses_bbox = multi_apply(
            self.loss_single_ignore,
            cls_scores,
            bbox_preds,
            labels_list,
            label_weights_list,
            bbox_targets_list,
            bbox_weights_list,
            labels_list_invalid,
            label_weights_list_invalid,
            bbox_targets_list_invalid,
            bbox_weights_list_invalid,
            img_metas=img_metas,
            num_total_samples=num_total_samples,
            cfg=cfg)

        return dict(loss_cls=losses_cls, loss_bbox=losses_bbox)

    @force_fp32(apply_to=('cls_scores', 'bbox_preds'))
    def loss_ignore_v1(self,
             cls_scores,
             bbox_preds,
             gt_bboxes,
             gt_labels,
             img_metas,
             cfg,
             gt_bboxes_ignore=None,
             old_output=None):
        featmap_sizes = [featmap.size()[-2:] for featmap in cls_scores]
        assert len(featmap_sizes) == len(self.anchor_generators)

        device = cls_scores[0].device

        anchor_list, valid_flag_list = self.get_anchors(
            featmap_sizes, img_metas, device=device)
        label_channels = self.cls_out_channels if self.use_sigmoid_cls else 1
        cls_reg_targets, cls_reg_targets_hard, cls_reg_targets_ignore = anchor_target_ignore_v1(
            anchor_list,
            valid_flag_list,
            gt_bboxes,
            img_metas,
            self.target_means,
            self.target_stds,
            cfg,
            gt_bboxes_ignore_list=gt_bboxes_ignore,
            gt_labels_list=gt_labels,
            label_channels=label_channels,
            sampling=self.sampling)
        if cls_reg_targets is None:
            return None

        #valid
        (labels_list, label_weights_list, bbox_targets_list, bbox_weights_list,
        num_total_pos, num_total_neg) = cls_reg_targets

        #hard
        (labels_list_invalid, label_weights_list_invalid,
         bbox_targets_list_invalid, bbox_weights_list_invalid) = cls_reg_targets_ignore

        #invalid
        (labels_list_hard) = cls_reg_targets_hard
        num_total_samples = (
            num_total_pos + num_total_neg if self.sampling else num_total_pos)
        losses_cls, losses_bbox = multi_apply(
            self.loss_single_ignore_v1,
            cls_scores,
            bbox_preds,
            labels_list,
            label_weights_list,
            bbox_targets_list,
            bbox_weights_list,
            labels_list_hard,
            labels_list_invalid,
            label_weights_list_invalid,
            bbox_targets_list_invalid,
            bbox_weights_list_invalid,
            img_metas=img_metas,
            num_total_samples=num_total_samples,
            cfg=cfg)

        return dict(loss_cls=losses_cls, loss_bbox=losses_bbox)




    @force_fp32(apply_to=('cls_scores', 'bbox_preds'))
    def loss(self,
             cls_scores,
             bbox_preds,
             gt_bboxes,
             gt_labels,
             img_metas,
             cfg,
             gt_bboxes_ignore=None,
             old_output=None):
        featmap_sizes = [featmap.size()[-2:] for featmap in cls_scores]
        assert len(featmap_sizes) == len(self.anchor_generators)

        device = cls_scores[0].device

        anchor_list, valid_flag_list = self.get_anchors(
            featmap_sizes, img_metas, device=device)
        label_channels = self.cls_out_channels if self.use_sigmoid_cls else 1
        if 'clean_foreground' not in cfg:
            cls_reg_targets = anchor_target(
                anchor_list,
                valid_flag_list,
                gt_bboxes,
                img_metas,
                self.target_means,
                self.target_stds,
                cfg,
                gt_bboxes_ignore_list=gt_bboxes_ignore,
                gt_labels_list=gt_labels,
                label_channels=label_channels,
                sampling=self.sampling)
            if cls_reg_targets is None:
                return None
            (labels_list, label_weights_list, bbox_targets_list, bbox_weights_list,
            num_total_pos, num_total_neg) = cls_reg_targets
            num_total_samples = (
                num_total_pos + num_total_neg if self.sampling else num_total_pos)
            if old_output == None:
                losses_cls, losses_bbox = multi_apply(
                    self.loss_single,
                    cls_scores,
                    bbox_preds,
                    labels_list,
                    label_weights_list,
                    bbox_targets_list,
                    bbox_weights_list,
                    img_metas=img_metas,
                    num_total_samples=num_total_samples,
                    cfg=cfg)

            else:
                losses_cls, losses_bbox, curr_losses = multi_apply(
                    self.loss_single_distill,
                    cls_scores,
                    bbox_preds,
                    labels_list,
                    label_weights_list,
                    bbox_targets_list,
                    bbox_weights_list,
                    old_output[0],
                    old_output[1],
                    img_metas=img_metas,
                    num_total_samples=num_total_samples,
                    cfg=cfg)
                return dict(loss_cls=losses_cls, loss_bbox=losses_bbox), dict(curr_loss=curr_losses)

        else:
            cls_reg_targets, cls_reg_targets_filter = anchor_target(
                anchor_list,
                valid_flag_list,
                gt_bboxes,
                img_metas,
                self.target_means,
                self.target_stds,
                cfg,
                gt_bboxes_ignore_list=gt_bboxes_ignore,
                gt_labels_list=gt_labels,
                label_channels=label_channels,
                sampling=self.sampling)
            if cls_reg_targets is None:
                return None
            (labels_list, label_weights_list, bbox_targets_list, bbox_weights_list,
            num_total_pos, num_total_neg) = cls_reg_targets
            (labels_list_filter, label_weights_list_filter, bbox_targets_list_filter, bbox_weights_list_filter,
             num_total_pos, num_total_neg) = cls_reg_targets_filter
            num_total_samples = (
                num_total_pos + num_total_neg if self.sampling else num_total_pos)
            losses_cls, losses_bbox = multi_apply(
                self.loss_single_filter,
                cls_scores,
                bbox_preds,
                labels_list,
                label_weights_list,
                bbox_targets_list,
                bbox_weights_list,
                labels_list_filter,
                label_weights_list_filter,
                bbox_targets_list_filter,
                bbox_weights_list_filter,
                img_metas=img_metas,
                num_total_samples=num_total_samples,
                cfg=cfg)

        return dict(loss_cls=losses_cls, loss_bbox=losses_bbox)

    @force_fp32(apply_to=('cls_scores', 'bbox_preds'))
    def get_bboxes(self,
                   cls_scores,
                   bbox_preds,
                   img_metas,
                   cfg,
                   rescale=False):
        """
        Transform network output for a batch into labeled boxes.

        Args:
            cls_scores (list[Tensor]): Box scores for each scale level
                Has shape (N, num_anchors * num_classes, H, W)
            bbox_preds (list[Tensor]): Box energies / deltas for each scale
                level with shape (N, num_anchors * 4, H, W)
            img_metas (list[dict]): size / scale info for each image
            cfg (mmcv.Config): test / postprocessing configuration
            rescale (bool): if True, return boxes in original image space

        Returns:
            list[tuple[Tensor, Tensor]]: each item in result_list is 2-tuple.
                The first item is an (n, 5) tensor, where the first 4 columns
                are bounding box positions (tl_x, tl_y, br_x, br_y) and the
                5-th column is a score between 0 and 1. The second item is a
                (n,) tensor where each item is the class index of the
                corresponding box.

        Example:
            >>> import mmcv
            >>> self = AnchorHead(num_classes=9, in_channels=1)
            >>> img_metas = [{'img_shape': (32, 32, 3), 'scale_factor': 1}]
            >>> cfg = mmcv.Config(dict(
            >>>     score_thr=0.00,
            >>>     nms=dict(type='nms', iou_thr=1.0),
            >>>     max_per_img=10))
            >>> feat = torch.rand(1, 1, 3, 3)
            >>> cls_score, bbox_pred = self.forward_single(feat)
            >>> # note the input lists are over different levels, not images
            >>> cls_scores, bbox_preds = [cls_score], [bbox_pred]
            >>> result_list = self.get_bboxes(cls_scores, bbox_preds,
            >>>                               img_metas, cfg)
            >>> det_bboxes, det_labels = result_list[0]
            >>> assert len(result_list) == 1
            >>> assert det_bboxes.shape[1] == 5
            >>> assert len(det_bboxes) == len(det_labels) == cfg.max_per_img
        """
        assert len(cls_scores) == len(bbox_preds)
        num_levels = len(cls_scores)

        device = cls_scores[0].device
        mlvl_anchors = [
            self.anchor_generators[i].grid_anchors(
                cls_scores[i].size()[-2:],
                self.anchor_strides[i],
                device=device) for i in range(num_levels)
        ]
        result_list = []
        for img_id in range(len(img_metas)):
            cls_score_list = [
                cls_scores[i][img_id].detach() for i in range(num_levels)
            ]
            bbox_pred_list = [
                bbox_preds[i][img_id].detach() for i in range(num_levels)
            ]
            img_shape = img_metas[img_id]['img_shape']
            scale_factor = img_metas[img_id]['scale_factor']
            proposals = self.get_bboxes_single(cls_score_list, bbox_pred_list,
                                               mlvl_anchors, img_shape,
                                               scale_factor, cfg, rescale)
            result_list.append(proposals)
        return result_list

    def get_bboxes_single(self,
                          cls_score_list,
                          bbox_pred_list,
                          mlvl_anchors,
                          img_shape,
                          scale_factor,
                          cfg,
                          rescale=False):
        """
        Transform outputs for a single batch item into labeled boxes.
        """
        assert len(cls_score_list) == len(bbox_pred_list) == len(mlvl_anchors)
        mlvl_bboxes = []
        mlvl_scores = []
        for cls_score, bbox_pred, anchors in zip(cls_score_list,
                                                 bbox_pred_list, mlvl_anchors):
            assert cls_score.size()[-2:] == bbox_pred.size()[-2:]
            cls_score = cls_score.permute(1, 2,
                                          0).reshape(-1, self.cls_out_channels)
            if self.use_sigmoid_cls:
                scores = cls_score.sigmoid()
            else:
                scores = cls_score.softmax(-1)
            bbox_pred = bbox_pred.permute(1, 2, 0).reshape(-1, 4)
            nms_pre = cfg.get('nms_pre', -1)
            if nms_pre > 0 and scores.shape[0] > nms_pre:
                # Get maximum scores for foreground classes.
                if self.use_sigmoid_cls:
                    max_scores, _ = scores.max(dim=1)
                else:
                    max_scores, _ = scores[:, 1:].max(dim=1)
                _, topk_inds = max_scores.topk(nms_pre)
                anchors = anchors[topk_inds, :]
                bbox_pred = bbox_pred[topk_inds, :]
                scores = scores[topk_inds, :]
            bboxes = delta2bbox(anchors, bbox_pred, self.target_means,
                                self.target_stds, img_shape)
            mlvl_bboxes.append(bboxes)
            mlvl_scores.append(scores)
        mlvl_bboxes = torch.cat(mlvl_bboxes)
        if rescale:
            mlvl_bboxes /= mlvl_bboxes.new_tensor(scale_factor)
        mlvl_scores = torch.cat(mlvl_scores)
        if self.use_sigmoid_cls:
            # Add a dummy background class to the front when using sigmoid
            padding = mlvl_scores.new_zeros(mlvl_scores.shape[0], 1)
            mlvl_scores = torch.cat([padding, mlvl_scores], dim=1)
        det_bboxes, det_labels = multiclass_nms(mlvl_bboxes, mlvl_scores,
                                                cfg.score_thr, cfg.nms,
                                                cfg.max_per_img)
        return det_bboxes, det_labels
