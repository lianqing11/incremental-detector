import torch.nn as nn

from mmdet.core import bbox2result
import torch
from .. import builder
from ..registry import DETECTORS
from .base import BaseDetector



def flip(x, dim):
    dim = x.dim() + dim if dim < 0 else dim
    return x[tuple(slice(None, None) if i!= dim
                   else torch.arange(x.size(i)-1, -1, -1).long()
                   for i in range(x.dim()))]



@DETECTORS.register_module
class SingleStageDetector(BaseDetector):
    """Base class for single-stage detectors.

    Single-stage detectors directly and densely predict bounding boxes on the
    output features of the backbone+neck.
    """

    def __init__(self,
                 backbone,
                 neck=None,
                 bbox_head=None,
                 train_cfg=None,
                 test_cfg=None,
                 pretrained=None):
        super(SingleStageDetector, self).__init__()
        self.backbone = builder.build_backbone(backbone)
        if neck is not None:
            self.neck = builder.build_neck(neck)
        self.bbox_head = builder.build_head(bbox_head)
        self.train_cfg = train_cfg
        self.test_cfg = test_cfg
        self.init_weights(pretrained=pretrained)

    def init_weights(self, pretrained=None):
        super(SingleStageDetector, self).init_weights(pretrained)
        self.backbone.init_weights(pretrained=pretrained)
        if self.with_neck:
            if isinstance(self.neck, nn.Sequential):
                for m in self.neck:
                    m.init_weights()
            else:
                self.neck.init_weights()
        self.bbox_head.init_weights()

    def extract_feat(self, img):
        """Directly extract features from the backbone+neck
        """
        x = self.backbone(img)
        if self.with_neck:
            x = self.neck(x)
        return x

    def forward_dummy(self, img):
        """Used for computing network flops.

        See `mmedetection/tools/get_flops.py`
        """
        x = self.extract_feat(img)
        outs = self.bbox_head(x)
        return outs

    def forward_train(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      gt_bboxes_ignore=None):
        x = self.extract_feat(img)
        outs = self.bbox_head(x)
        loss_inputs = outs + (gt_bboxes, gt_labels, img_metas, self.train_cfg)
        if 'noisy_ablation_v1' in self.train_cfg:
            losses = self.bbox_head.loss_ignore_v1(*loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
        elif 'oracle_ignore' and 'noisy_ablation' not in self.train_cfg:
            losses = self.bbox_head.loss(
                *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
            return losses
        else:
            losses = self.bbox_head.loss_ignore(
                *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
            return losses


    def forward_train_feature_map(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      flip=False,
                      gt_bboxes_ignore=None):
        if flip == True:
            img_flip = flip(img.clone(), 3)
            img = img_flip
        x = self.extract_feat(img)
        outs = self.bbox_head(x)
        if flip == True:
            outs_flip = list(outs)
            for i in range(len(outs_flip)):
                for k in range(len(outs_flip[i])):
                    outs_flip[i][k] = flip(outs_flip[i][k], 3)
            outs = outs_flip
        return outs

    def forward_train_student(self,
                              img,
                              img_metas,
                              gt_bboxes,
                              gt_labels,
                              gt_bboxes_ignore=None,
                              old_output=None):
        x = self.extract_feat(img)
        outs = self.bbox_head(x)
        loss_inputs = outs + (gt_bboxes, gt_labels, img_metas, self.train_cfg)
        losses, curr_losses = self.bbox_head.loss(
            *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore, old_output=old_output)
        return losses, curr_losses


    def forward_train_flip(self,
                           img,
                           img_metas,
                           gt_bboxes,
                           gt_labels,
                           gt_bboxes_ignore=None):
        img_flip = flip(img.clone(), 3)
        x = self.extract_feat(img)
        outs = self.bbox_head(x)
        loss_inputs = outs + (gt_bboxes, gt_labels, img_metas, self.train_cfg)
        losses = self.bbox_head.loss(
            *loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)

        x_flip = self.extract_feat(img_flip)
        outs_flip = self.bbox_head(x_flip)
        #flip outs
        outs_flip = list(outs_flip)
        for i in range(len(outs_flip)):
            for k in range(len(outs_flip[i])):
                outs_flip[i][k] = flip(outs_flip[i][k], 3)
        return losses, outs, outs_flip


    def simple_test(self, img, img_meta, rescale=False, **kwargs):
        x = self.extract_feat(img)
        outs = self.bbox_head(x)
        bbox_inputs = outs + (img_meta, self.test_cfg, rescale)
        bbox_list = self.bbox_head.get_bboxes(*bbox_inputs)
        bbox_results = [
            bbox2result(det_bboxes, det_labels, self.bbox_head.num_classes)
            for det_bboxes, det_labels in bbox_list
        ]
        return bbox_results[0]

    def aug_test(self, imgs, img_metas, rescale=False):
        raise NotImplementedError
