#!/bin/bash
for teacher_alpha in {0.9,0.5}
do
    echo $teacher_alpha
    bash ./tools/dist_train.sh train_mean_teacher_distill_weight.py incremental_cfgs/retinanet/teacher/distill_teacher_weight_1x.py 2 19323 --teacher_alpha $teacher_alpha --work_dir tune_hyperparam/retinanet_distill_teacher_alpha-$teacher_alpha
done

